<?php /** @noinspection PhpComposerExtensionStubsInspection */

namespace Mouf\NodeJsInstaller;


abstract class System
{
	/** @var null|bool */
	private static $isWindowsOsCache = null;
	
	
	/**
	 *
	 */
	public static function fixMaxExecutionTime()
	{
		\ini_set('max_execution_time', 0);
		\ini_set('memory_limit', -1);
		\ini_set('pcre.backtrack_limit', 1000 * 1000);
		\ini_set('pcre.recursion_limit', 500 * 1000);
	}
	
	/**
	 * @return bool
	 */
	public static function isWindowsOs()
	{
		if(self::$isWindowsOsCache === null)
		{
			self::$isWindowsOsCache = (\mb_strtoupper(\substr(\PHP_OS, 0, 3)) === 'WIN');
		}
		return self::$isWindowsOsCache;
	}
	
	/**
	 * Makes warnings and notices throw an exception instead of echo'ing their error message.
	 */
	public static function fixErrorHandler()
	{
		\set_error_handler(static function($errno, $errstr, $errfile, $errline, $errcontext = null)
		{
			if(!(\error_reporting() & $errno))
			{
				return;
			}
			switch($errno)
			{
				case \E_USER_ERROR:
					$type = 'Fatal Error';
					break;
				case \E_USER_WARNING:
				case \E_WARNING:
					$type = 'Warning';
					break;
				case \E_USER_NOTICE:
				case \E_NOTICE:
				case @\E_STRICT:
					$type = 'Notice';
					break;
				case @\E_RECOVERABLE_ERROR:
					$type = 'Catchable';
					break;
				default:
					$type = 'Unknown Error';
					break;
			}
			/** @noinspection PhpUnhandledExceptionInspection */
			throw new \ErrorException($type . ': ' . $errstr, 0, $errno, $errfile, $errline);
		});
	}
	
	/**
	 * Provide a Java style exception trace.
	 *
	 * @param \Throwable $e
	 * @param array|null $seen
	 *
	 * @return string
	 */
	public static function getExceptionTrace($e, $seen = null)
	{
		$starter = $seen ? 'Caused by: ' : '';
		$result = [];
		if(!$seen)
		{
			$seen = [];
		}
		$trace = $e->getTrace();
		$prev = $e->getPrevious();
		$result[] = \sprintf('%s%s: %s', $starter, \get_class($e), $e->getMessage());
		$file = $e->getFile();
		$line = $e->getLine();
		while(true)
		{
//			$current = "$file:$line";
//			if(\is_array($seen) && \in_array($current, $seen))
//			{
//				$result[] = \sprintf(' ... %d more', \count($trace) + 1);
//				break;
//			}
			/** @noinspection NullCoalescingOperatorCanBeUsedInspection */
			$result[] = \sprintf(' at %s%s%s(%s%s%s)',
				\count($trace) && \array_key_exists('class', $trace[0]) ? \str_replace('\\', '.', $trace[0]['class']) : '',
				\count($trace) && \array_key_exists('class', $trace[0]) && \array_key_exists('function', $trace[0]) ? '.' : '',
				\count($trace) && \array_key_exists('function', $trace[0]) ? \str_replace('\\', '.', $trace[0]['function']) : '(main)',
				$line === null ? $file : \basename($file),
				$line === null ? '' : ':',
				$line === null ? '' : $line);
			if(\is_array($seen))
			{
				$seen[] = "$file:$line";
			}
			if(!\count($trace))
			{
				break;
			}
			$file = \array_key_exists('file', $trace[0]) ? $trace[0]['file'] : 'Unknown Source';
			$line = \array_key_exists('file', $trace[0]) && \array_key_exists('line', $trace[0]) && $trace[0]['line'] ? $trace[0]['line'] : null;
			\array_shift($trace);
		}
		$result = \implode("\n", $result);
		if($prev)
		{
			$result .= "\n" . self::getExceptionTrace($prev, $seen);
		}
		return $result;
	}
	
	/**
	 * @param mixed $data
	 *
	 * @return string
	 */
	public static function jsonEncode($data)
	{
		$result = \json_encode($data, \JSON_PARTIAL_OUTPUT_ON_ERROR);
		if($result !== false)
		{
			return $result;
		}
		throw new \RuntimeException('json encode failed: ' . \json_last_error_msg());
	}
	
	/**
	 * @param mixed $data
	 *
	 * @return string
	 */
	public static function jsonEncodePretty($data)
	{
		$result = \json_encode($data, \JSON_PRETTY_PRINT | \JSON_PARTIAL_OUTPUT_ON_ERROR);
		if($result !== false)
		{
			return $result;
		}
		throw new \RuntimeException('json encode failed: ' . \json_last_error_msg());
	}
	
	/**
	 * @return string
	 */
	public static function generateUniqueName()
	{
		$parts = \explode(' ', (string) \microtime(false));
		$secPart = $parts[0];
		/** @noinspection MultiAssignmentUsageInspection */
		$sec = $parts[1];
		return $sec . '_' . \substr($secPart, 2, 6) . '_' . mt_rand();
	}
	
	/**
	 * @param string   $filePath
	 * @param float    $secondsRetrying
	 * @param float    $triesPerSecond
	 * @param callable $action
	 *
	 * @return mixed
	 * @throws \Throwable
	 */
	public static function synchronizeOnFile($filePath, $secondsRetrying, $triesPerSecond, $action)
	{
		$filePath = self::normalizePathSlashes($filePath);
		$sleepTime = ($triesPerSecond <= 0) ? 1000 : (int) (1000000 / $triesPerSecond);
		if($sleepTime < 1000)
		{
			$sleepTime = 1000;// 1 ms
		}
		
		if(\function_exists('hrtime'))
		{
			$startTime = \hrtime(true);
			while(true)
			{
				$result = self::_synchronizeOnFile($filePath, $action);
				if($result !== false)
				{
					return $result['result'];
				}
				$timePastInSeconds = ((\hrtime(true) - $startTime) / 1000000000);
				if($timePastInSeconds >= $secondsRetrying)
				{
					throw new \RuntimeException('could not acquire file lock on ' . $filePath);
				}
				\usleep($sleepTime);
			}
		}
		else
		{
			$startTime = \microtime(true);
			while(true)
			{
				$result = self::_synchronizeOnFile($filePath, $action);
				if($result !== false)
				{
					return $result['result'];
				}
				$timePastInSeconds = (\microtime(true) - $startTime);
				if($timePastInSeconds >= $secondsRetrying)
				{
					throw new \RuntimeException('could not acquire file lock on ' . $filePath);
				}
				\usleep($sleepTime);
			}
		}
	}
	
	/**
	 * @param string   $filePath
	 * @param callable $action
	 *
	 * @return array|false
	 * @throws \Throwable
	 */
	private static function _synchronizeOnFile($filePath, $action)
	{
		$fileHandler = \fopen($filePath, 'cb');
		if($fileHandler === false)
		{
			throw new \RuntimeException('could not acquire file handler on ' . $filePath);
		}
		
		if(!\flock($fileHandler, \LOCK_EX | \LOCK_NB))
		{
			\fclose($fileHandler);
			return false;
		}
		
		self::fixFilePermissions($filePath);
		try
		{
			$result = $action();
			\flock($fileHandler, \LOCK_UN);
			\fclose($fileHandler);
			return ['result' => $result];
		}
		catch(\Throwable $e)
		{
			\flock($fileHandler, \LOCK_UN);
			\fclose($fileHandler);
			throw $e;
		}
	}
	
	/**
	 * @param string $dirPath
	 * @param int    $retriesRemaining
	 *
	 * @throws \RuntimeException
	 */
	public static function emptyDir($dirPath, $retriesRemaining = 30)
	{
		$dirPath = self::normalizePathSlashes($dirPath);
		$initialRetriesRemaining = $retriesRemaining;
		
		if(!\is_dir($dirPath))
		{
			self::createDir($dirPath, $retriesRemaining);
			return;
		}
		if(($dirPath === '') || ($dirPath === '/'))
		{
			return;
		}
		$lastChar = \mb_substr($dirPath, -1);
		if($lastChar !== '/')
		{
			$dirPath .= '/';
		}
		
		while(true)
		{
			$foundE = null;
			try
			{
				self::fixFilePermissions($dirPath);
				if(\is_dir($dirPath))
				{
					$files = \array_diff(\scandir($dirPath, \SCANDIR_SORT_NONE), ['.', '..']);
					if(!empty($files))
					{
						foreach($files as $file)
						{
							$filePath = $dirPath . $file;
							self::deleteDir($filePath . '/', 0);
							self::deleteFile($filePath, 0);
						}
					}
					else
					{
						return;
					}
				}
				else
				{
					self::createDir($dirPath, 0);
					return;
				}
			}
			catch(\Throwable $e)
			{
				$foundE = $e;
			}
			
			if(!\is_dir($dirPath))
			{
				self::createDir($dirPath, $initialRetriesRemaining);
				return;
			}
			if(empty(\array_diff(\scandir($dirPath, \SCANDIR_SORT_NONE), ['.', '..'])))
			{
				return;
			}
			if($retriesRemaining <= 0)
			{
				throw new \RuntimeException('dir couldn\'t be emptied: ' . $dirPath . (($foundE === null) ? '' : ', reason: ' . $foundE->getMessage()));
			}
			$retriesRemaining--;
			\usleep(100000);// 100 millis
		}
	}
	
	/**
	 * @param string $dirPath
	 *
	 * @return bool
	 */
	public static function isEmptyDir($dirPath)
	{
		$dirPath = self::normalizePathSlashes($dirPath);
		if(\is_dir($dirPath))
		{
			$files = \array_diff(\scandir($dirPath, \SCANDIR_SORT_NONE), ['.', '..']);
			return empty($files);
		}
		return true;
	}
	
	/**
	 * @param string $dirPath
	 * @param int    $retriesRemaining
	 *
	 * @throws \RuntimeException
	 */
	public static function deleteDir($dirPath, $retriesRemaining = 30)
	{
		$dirPath = self::normalizePathSlashes($dirPath);
		
		if(!\is_dir($dirPath))
		{
			return;
		}
		if(($dirPath === '') || ($dirPath === '/'))
		{
			return;
		}
		$lastChar = \mb_substr($dirPath, -1);
		if($lastChar !== '/')
		{
			$dirPath .= '/';
		}
		
		while(true)
		{
			$foundE = null;
			try
			{
				self::fixFilePermissions($dirPath);
				if(\is_link($dirPath))
				{
					@\unlink($dirPath);
				}
				if(\is_dir($dirPath))
				{
					@\rmdir($dirPath);
				}
				else
				{
					return;
				}
				if(\is_dir($dirPath))
				{
					$files = \array_diff(\scandir($dirPath, \SCANDIR_SORT_NONE), ['.', '..']);
					foreach($files as $file)
					{
						$filePath = $dirPath . $file;
						self::deleteDir($filePath . '/', 0);
						self::deleteFile($filePath, 0);
					}
					\rmdir($dirPath);
				}
				else
				{
					return;
				}
			}
			catch(\Throwable $e)
			{
				$foundE = $e;
			}
			
			if(!\is_dir($dirPath))
			{
				return;
			}
			if($retriesRemaining <= 0)
			{
				throw new \RuntimeException('dir couldn\'t be deleted: ' . $dirPath . (($foundE === null) ? '' : ', reason: ' . $foundE->getMessage()));
			}
			$retriesRemaining--;
			\usleep(100000);// 100 millis
		}
	}
	
	/**
	 * @param string $filePath
	 * @param int    $retriesRemaining
	 *
	 * @throws \RuntimeException
	 */
	public static function deleteFile($filePath, $retriesRemaining = 30)
	{
		$filePath = self::normalizePathSlashes($filePath);
		
		if(!\is_link($filePath) && !\is_file($filePath))
		{
			return;
		}
		
		while(true)
		{
			$foundE = null;
			try
			{
				self::fixFilePermissions($filePath);
				if(\is_link($filePath) || \is_file($filePath))
				{
					\unlink($filePath);
				}
				else
				{
					return;
				}
			}
			catch(\Throwable $e)
			{
				$foundE = $e;
			}
			
			if(!\is_link($filePath) && !\is_file($filePath))
			{
				return;
			}
			if($retriesRemaining <= 0)
			{
				throw new \RuntimeException('file couldn\'t be deleted: ' . $filePath . (($foundE === null) ? '' : ', reason: ' . $foundE->getMessage()));
			}
			$retriesRemaining--;
			\usleep(100000);// 100 millis
		}
	}
	
	/**
	 * @param string $linkPath
	 * @param int    $retriesRemaining
	 *
	 * @throws \RuntimeException
	 */
	public static function deleteLink($linkPath, $retriesRemaining = 30)
	{
		$linkPath = self::normalizePathSlashes($linkPath);
		
		if(!\is_link($linkPath))
		{
			return;
		}
		
		while(true)
		{
			$foundE = null;
			try
			{
				self::fixFilePermissions($linkPath);
				if(\is_link($linkPath))
				{
					\unlink($linkPath);
				}
				else
				{
					return;
				}
			}
			catch(\Throwable $e)
			{
				$foundE = $e;
			}
			
			if(!\is_link($linkPath))
			{
				return;
			}
			if($retriesRemaining <= 0)
			{
				throw new \RuntimeException('link couldn\'t be deleted: ' . $linkPath . (($foundE === null) ? '' : ', reason: ' . $foundE->getMessage()));
			}
			$retriesRemaining--;
			\usleep(100000);// 100 millis
		}
	}
	
	/**
	 * @param string $dirPathFrom
	 * @param string $dirPathTo
	 * @param int    $retriesRemaining
	 *
	 * @throws \RuntimeException
	 */
	public static function copyDir($dirPathFrom, $dirPathTo, $retriesRemaining = 30)
	{
		$dirPathFrom = self::normalizePathSlashes($dirPathFrom);
		$dirPathTo = self::normalizePathSlashes($dirPathTo);
		
		if(!\is_dir($dirPathFrom))
		{
			return;
		}
		
		if(\is_dir($dirPathTo))
		{
			self::emptyDir($dirPathTo, $retriesRemaining);
		}
		elseif(!\is_dir($dirPathTo))
		{
			self::createDir($dirPathTo, $retriesRemaining);
		}
		
		$lastChar = \mb_substr($dirPathFrom, -1);
		if($lastChar !== '/')
		{
			$dirPathFrom .= '/';
		}
		
		$lastChar = \mb_substr($dirPathTo, -1);
		if($lastChar !== '/')
		{
			$dirPathTo .= '/';
		}
		
		while(true)
		{
			self::fixFilePermissions($dirPathFrom);
			self::fixFilePermissions($dirPathTo);
			
			$foundE = null;
			try
			{
				$files = \array_diff(\scandir($dirPathFrom, \SCANDIR_SORT_NONE), ['.', '..']);
				if(!empty($files))
				{
					foreach($files as $file)
					{
						$filePathFrom = $dirPathFrom . $file;
						$filePathTo = $dirPathFrom . $file;
						self::copyDir($filePathFrom . '/', $filePathTo . '/', 0);
						self::copyFile($filePathFrom, $filePathTo);
					}
				}
				return;
			}
			catch(\Throwable $e)
			{
				$foundE = $e;
			}
			
			if($retriesRemaining <= 0)
			{
				throw new \RuntimeException('couldn\'t copy dir: ' . $dirPathFrom . ' to ' . $dirPathTo . (($foundE === null) ? '' : ', reason: ' . $foundE->getMessage()));
			}
			$retriesRemaining--;
			\usleep(100000);// 100 millis
		}
	}
	
	/**
	 * @param string $dirPathFrom
	 * @param string $dirPathTo
	 * @param int    $retriesRemaining
	 *
	 * @throws \RuntimeException
	 */
	public static function moveDir($dirPathFrom, $dirPathTo, $retriesRemaining = 30)
	{
		$dirPathFrom = self::normalizePathSlashes($dirPathFrom);
		$dirPathTo = self::normalizePathSlashes($dirPathTo);
		$initialRetriesRemaining = $retriesRemaining;
		
		while(true)
		{
			self::fixFilePermissions($dirPathFrom);
			self::fixFilePermissions(self::mb_dirname($dirPathTo));
			
			$foundE = null;
			$result = false;
			try
			{
				$result = \rename($dirPathFrom, $dirPathTo);
			}
			catch(\Throwable $e)
			{
				$foundE = $e;
			}
			
			if($result)
			{
				return;
			}
			if(self::stringContains($foundE->getMessage(), 'copy()'))
			{
				self::moveDirFallback($dirPathFrom, $dirPathTo, $initialRetriesRemaining);
				return;
			}
			if($retriesRemaining <= 0)
			{
				throw new \RuntimeException('couldn\'t move dir: ' . $dirPathFrom . ' to ' . $dirPathTo . (($foundE === null) ? '' : ', reason: ' . $foundE->getMessage()));
			}
			$retriesRemaining--;
			\usleep(100000);// 100 millis
		}
	}
	
	/**
	 * @param string $dirPathFrom
	 * @param string $dirPathTo
	 * @param int    $retriesRemaining
	 *
	 * @throws \RuntimeException
	 */
	protected static function moveDirFallback($dirPathFrom, $dirPathTo, $retriesRemaining = 30)
	{
		$dirPathFrom = self::normalizePathSlashes($dirPathFrom);
		$dirPathTo = self::normalizePathSlashes($dirPathTo);
		
		$foundE = null;
		try
		{
			self::deleteDir($dirPathTo, $retriesRemaining);
			self::copyDir($dirPathFrom, $dirPathTo, $retriesRemaining);
			self::deleteDir($dirPathFrom, $retriesRemaining);
			return;
		}
		catch(\Throwable $e)
		{
			$foundE = $e;
		}
		
		throw new \RuntimeException('couldn\'t move dir: ' . $dirPathFrom . ' to ' . $dirPathTo . (($foundE === null) ? '' : ', reason: ' . $foundE->getMessage()));
	}
	
	/**
	 * @param string $dirPath
	 * @param int    $retriesRemaining
	 *
	 * @throws \RuntimeException
	 */
	public static function createDir($dirPath, $retriesRemaining = 30)
	{
		$dirPath = self::normalizePathSlashes($dirPath);
		$dirPathParent = self::mb_dirname($dirPath);
		
		if(self::isWindowsOs())
		{
			while(true)
			{
				self::fixFilePermissions($dirPathParent);
				$foundE = null;
				$result = false;
				try
				{
					$result = \mkdir($dirPath, 0777, true);
				}
				catch(\Throwable $e)
				{
					$foundE = $e;
				}
				
				if($result || \is_dir($dirPath))
				{
					return;
				}
				if($retriesRemaining <= 0)
				{
					throw new \RuntimeException('couldn\'t create dir: ' . $dirPath . (($foundE === null) ? '' : ', reason: ' . $foundE->getMessage()));
				}
				$retriesRemaining--;
				\usleep(100000);// 100 millis
			}
		}
		
		$dirPathParts = \array_filter(\mb_split('/', $dirPath));
		$retry = true;
		while($retry)
		{
			self::fixFilePermissions($dirPathParent);
			$dirPath = '';
			$retry = false;
			foreach($dirPathParts as $dirPathPart)
			{
				$dirPath .= '/' . $dirPathPart;
				if(\is_dir($dirPath))
				{
					continue;
				}
				
				$foundE = null;
				$result = false;
				try
				{
					$result = \mkdir($dirPath, 0777, true);
				}
				catch(\Throwable $e)
				{
					$foundE = $e;
				}
				
				if($result || \is_dir($dirPath))
				{
					self::fixFilePermissions($dirPath);
					continue;
				}
				if($retriesRemaining <= 0)
				{
					throw new \RuntimeException('couldn\'t create dir: ' . $dirPath . (($foundE === null) ? '' : ', reason: ' . $foundE->getMessage()));
				}
				$retriesRemaining--;
				\usleep(100000);// 100 millis
				$retry = true;
				break;
			}
		}
	}
	
	/**
	 * @param string      $filePath
	 * @param string|null $defaultValue
	 *
	 * @return string
	 * @throws \RuntimeException
	 */
	public static function getFileContent($filePath, $defaultValue = null)
	{
		$filePath = self::normalizePathSlashes($filePath);
		self::fixFilePermissions($filePath);
		if(!\is_file($filePath))
		{
			if($defaultValue === null)
			{
				throw new \RuntimeException('file doesn\'t exist: ' . $filePath);
			}
			return $defaultValue;
		}
		try
		{
			$result = @\file_get_contents($filePath);
		}
		catch(\Throwable $e)
		{
			$result = false;
		}
		if(!\is_string($result))
		{
			if($defaultValue === null)
			{
				throw new \RuntimeException('file doesn\'t exist: ' . $filePath);
			}
			return $defaultValue;
		}
		return $result;
	}
	
	/**
	 * @param string $filePath
	 * @param string $string
	 */
	public static function setFileContent($filePath, $string)
	{
		$filePath = self::normalizePathSlashes($filePath);
		self::fixFilePermissions(self::mb_dirname($filePath));
		self::fixFilePermissions($filePath);
		\file_put_contents($filePath, $string);
		self::fixFilePermissions($filePath);
	}
	
	/**
	 * @param string $fromFilePath
	 * @param string $toFilePath
	 */
	public static function copyFile($fromFilePath, $toFilePath)
	{
		$fromFilePath = self::normalizePathSlashes($fromFilePath);
		$toFilePath = self::normalizePathSlashes($toFilePath);
		if(!\is_file($fromFilePath))
		{
			return;
		}
		self::fixFilePermissions($fromFilePath);
		self::fixFilePermissions(self::mb_dirname($toFilePath));
		self::fixFilePermissions($toFilePath);
		try
		{
			\copy($fromFilePath, $toFilePath);
		}
		catch(\Throwable $e)
		{
			if(\is_file($fromFilePath))
			{
				throw new \RuntimeException($e->getMessage(), $e->getCode(), $e);
			}
		}
		self::fixFilePermissions($toFilePath);
	}
	
	/**
	 * @param string $fromFilePath
	 * @param string $toFilePath
	 */
	public static function linkFile($fromFilePath, $toFilePath)
	{
		$fromFilePath = self::normalizePathSlashes($fromFilePath);
		$toFilePath = self::normalizePathSlashes($toFilePath);
		if(!\is_file($fromFilePath))
		{
			return;
		}
		self::fixFilePermissions($fromFilePath);
		self::fixFilePermissions(self::mb_dirname($toFilePath));
		self::fixFilePermissions($toFilePath);
		try
		{
			\link($fromFilePath, $toFilePath);
		}
		catch(\Throwable $e)
		{
		}
		self::fixFilePermissions($toFilePath);
		if(\is_file($toFilePath))
		{
			return;
		}
		try
		{
			\copy($fromFilePath, $toFilePath);
		}
		catch(\Throwable $e)
		{
			if(\is_file($fromFilePath))
			{
				throw new \RuntimeException($e->getMessage(), $e->getCode(), $e);
			}
		}
		self::fixFilePermissions($toFilePath);
	}
	
	/**
	 * @param string $fromFilePath
	 * @param string $toFilePath
	 * @param int    $retriesRemaining
	 *
	 * @throws \RuntimeException
	 */
	public static function moveFile($fromFilePath, $toFilePath, $retriesRemaining = 30)
	{
		$fromFilePath = self::normalizePathSlashes($fromFilePath);
		$toFilePath = self::normalizePathSlashes($toFilePath);
		
		while(true)
		{
			self::fixFilePermissions($fromFilePath);
			self::fixFilePermissions(self::mb_dirname($toFilePath));
			self::fixFilePermissions($toFilePath);
			
			$foundE = null;
			$result = false;
			try
			{
				$result = \rename($fromFilePath, $toFilePath);
			}
			catch(\Throwable $e)
			{
				$foundE = $e;
			}
			
			if($result)
			{
				return;
			}
			if($retriesRemaining <= 0)
			{
				throw new \RuntimeException('couldn\'t move file: ' . $fromFilePath . ' to ' . $toFilePath . (($foundE === null) ? '' : ', reason: ' . $foundE->getMessage()));
			}
			$retriesRemaining--;
			\usleep(100000);// 100 millis
		}
	}
	
	/**
	 * @param array $array
	 */
	public static function sortKeys(&$array)
	{
		\uksort($array, [get_class(), '_sortKeysCompare']);
	}
	
	/**
	 * @param string $a
	 * @param string $b
	 *
	 * @return int
	 */
	private static function _sortKeysCompare($a, $b)
	{
		$aParts = \explode('/', $a);
		$bParts = \explode('/', $b);
		
		$aIsDir = \count($aParts) > 1;
		$bIsDir = \count($bParts) > 1;
		
		if($aIsDir && $bIsDir)
		{
			return self::_sortKeysCompareArray($aParts, $bParts);
		}
		if(!$aIsDir && !$bIsDir)
		{
			return self::_sortKeysCompareString($a, $b);
		}
		return $aIsDir ? -1 : 1;
	}
	
	/**
	 * @param string $a
	 * @param string $b
	 *
	 * @return int
	 */
	private static function _sortKeysCompareString($a, $b)
	{
		return \strcasecmp(\strtolower($a), \strtolower($b));
	}
	
	/**
	 * @param array $aParts
	 * @param array $bParts
	 *
	 * @return int
	 */
	private static function _sortKeysCompareArray($aParts, $bParts)
	{
		for($i = 0; $i < \min(\count($aParts), \count($bParts)); $i++)
		{
			$cmp = self::_sortKeysCompareString($aParts[$i], $bParts[$i]);
			if($cmp !== 0)
			{
				return $cmp;
			}
		}
		return \count($aParts) - \count($bParts);
	}
	
	/**
	 * @param string        $dirPath
	 * @param callable|null $action A callable with parameters: string $relativeDirPath, string $fileName, string $fullFilePath
	 * @param boolean       $sorted Whether to sort or not
	 *
	 * @returns array|null
	 * @throws \RuntimeException
	 */
	public static function loopThroughDir($dirPath, $action = null, $sorted = true)
	{
		$fixedDirPath = \realpath($dirPath);
		if($fixedDirPath === false)
		{
			return (($action === null) ? [] : null);
		}
		$fixedDirPath = self::normalizePathSlashes($fixedDirPath);
		if(!self::stringEndsWith($fixedDirPath, '/'))
		{
			$fixedDirPath .= '/';
		}
		$filesData = [];
		self::_loopThroughDir($fixedDirPath, $filesData);
		if($sorted)
		{
			self::sortKeys($filesData);
		}
		if($action !== null)
		{
			foreach($filesData as $fileData)
			{
				$action($fileData[0], $fileData[1], $fileData[2]);
			}
			return null;
		}
		return $filesData;
	}
	
	/**
	 * @param string $dirPath
	 * @param array  $filesData
	 *
	 * @throws \RuntimeException
	 */
	private static function _loopThroughDir($dirPath, &$filesData)
	{
		try
		{
			$filenames = \scandir($dirPath, \SCANDIR_SORT_NONE);
			if($filenames === false)
			{
				return;
			}
			foreach($filenames as $filename)
			{
				try
				{
					if(($filename === '.') || ($filename === '..'))
					{
						continue;
					}
					$filePath = $dirPath . $filename;
					if(\is_file($filePath))
					{
						$filesData[$filePath] = ['/', $filename, $filePath];
					}
				}
				catch(\Throwable $e)
				{
				}
			}
		}
		catch(\Throwable $e)
		{
		}
	}
	
	/**
	 * @param string        $dirPath
	 * @param callable|null $action A callable with parameters: string $relativeDirPath, string $fileName, string $fullFilePath
	 * @param boolean       $sorted Whether to sort or not
	 *
	 * @returns array|null
	 * @throws \RuntimeException
	 */
	public static function loopThroughDirRecursive($dirPath, $action = null, $sorted = true)
	{
		$fixedDirPath = \realpath($dirPath);
		if($fixedDirPath === false)
		{
			return (($action === null) ? [] : null);
		}
		$fixedDirPath = self::normalizePathSlashes($fixedDirPath);
		if(!self::stringEndsWith($fixedDirPath, '/'))
		{
			$fixedDirPath .= '/';
		}
		$filesData = [];
		self::_loopThroughDirRecursive($fixedDirPath, '', $filesData);
		if($sorted)
		{
			self::sortKeys($filesData);
		}
		if($action !== null)
		{
			foreach($filesData as $fileData)
			{
				$action($fileData[0], $fileData[1], $fileData[2]);
			}
			return null;
		}
		return $filesData;
	}
	
	/**
	 * @param string $dirPath
	 * @param string $relativeDir
	 * @param array  $filesData
	 *
	 * @throws \RuntimeException
	 */
	private static function _loopThroughDirRecursive($dirPath, $relativeDir, &$filesData)
	{
		try
		{
			$filenames = \scandir($dirPath, \SCANDIR_SORT_NONE);
			if($filenames === false)
			{
				return;
			}
			foreach($filenames as $filename)
			{
				try
				{
					if(($filename === '.') || ($filename === '..'))
					{
						continue;
					}
					$filePath = $dirPath . $filename;
					if(\is_dir($filePath))
					{
						self::_loopThroughDirRecursive($filePath . '/', $relativeDir . $filename . '/', $filesData);
					}
					else
					{
						$filesData[$filePath] = [($relativeDir === '') ? '/' : $relativeDir, $filename, $filePath];
					}
				}
				catch(\Throwable $e)
				{
				}
			}
		}
		catch(\Throwable $e)
		{
		}
	}
	
	/**
	 * @param string        $dirPath
	 * @param callable|null $action A callable with parameters: string $relativeDirPath, string $dirName, string $fullDirPath
	 * @param boolean       $sorted Whether to sort or not
	 *
	 * @returns array|null
	 * @throws \RuntimeException
	 */
	public static function loopThroughDir_dirs($dirPath, $action = null, $sorted = true)
	{
		$fixedDirPath = \realpath($dirPath);
		if($fixedDirPath === false)
		{
			return (($action === null) ? [] : null);
		}
		$fixedDirPath = self::normalizePathSlashes($fixedDirPath);
		if(!self::stringEndsWith($fixedDirPath, '/'))
		{
			$fixedDirPath .= '/';
		}
		$filesData = [];
		self::_loopThroughDir_dirs($fixedDirPath, $filesData);
		if($sorted)
		{
			self::sortKeys($filesData);
		}
		if($action !== null)
		{
			foreach($filesData as $fileData)
			{
				$action($fileData[0], $fileData[1], $fileData[2]);
			}
			return null;
		}
		return $filesData;
	}
	
	/**
	 * @param string $dirPath
	 * @param array  $filesData
	 *
	 * @throws \RuntimeException
	 */
	private static function _loopThroughDir_dirs($dirPath, &$filesData)
	{
		try
		{
			$filenames = \scandir($dirPath, \SCANDIR_SORT_NONE);
			if($filenames === false)
			{
				return;
			}
			foreach($filenames as $filename)
			{
				try
				{
					if(($filename === '.') || ($filename === '..'))
					{
						continue;
					}
					$filePath = $dirPath . $filename;
					if(\is_dir($filePath))
					{
						$filesData[$filePath] = ['/', $filename, $filePath];
					}
				}
				catch(\Throwable $e)
				{
				}
			}
		}
		catch(\Throwable $e)
		{
		}
	}
	
	/**
	 * @param string        $dirPath
	 * @param callable|null $action A callable with parameters: string $relativeDirPath, string $dirName, string $fullDirPath
	 * @param boolean       $sorted Whether to sort or not
	 *
	 * @returns array|null
	 * @throws \RuntimeException
	 */
	public static function loopThroughDirRecursive_dirs($dirPath, $action = null, $sorted = true)
	{
		$fixedDirPath = \realpath($dirPath);
		if($fixedDirPath === false)
		{
			return (($action === null) ? [] : null);
		}
		$fixedDirPath = self::normalizePathSlashes($fixedDirPath);
		if(!self::stringEndsWith($fixedDirPath, '/'))
		{
			$fixedDirPath .= '/';
		}
		$filesData = [];
		self::_loopThroughDirRecursive_dirs($fixedDirPath, '', $filesData);
		if($sorted)
		{
			self::sortKeys($filesData);
		}
		if($action !== null)
		{
			foreach($filesData as $fileData)
			{
				$action($fileData[0], $fileData[1], $fileData[2]);
			}
			return null;
		}
		return $filesData;
	}
	
	/**
	 * @param string $dirPath
	 * @param string $relativeDir
	 * @param array  $filesData
	 *
	 * @throws \RuntimeException
	 */
	private static function _loopThroughDirRecursive_dirs($dirPath, $relativeDir, &$filesData)
	{
		try
		{
			$filenames = \scandir($dirPath, \SCANDIR_SORT_NONE);
			if($filenames === false)
			{
				return;
			}
			foreach($filenames as $filename)
			{
				try
				{
					if(($filename === '.') || ($filename === '..'))
					{
						continue;
					}
					$filePath = $dirPath . $filename;
					if(\is_dir($filePath))
					{
						$filesData[$filePath] = [($relativeDir === '') ? '/' : $relativeDir, $filename, $filePath];
						self::_loopThroughDirRecursive_dirs($filePath . '/', $relativeDir . $filename . '/', $filesData);
					}
				}
				catch(\Throwable $e)
				{
				}
			}
		}
		catch(\Throwable $e)
		{
		}
	}
	
	/**
	 * @param string       $string
	 * @param string|array $startsWith
	 *
	 * @return bool
	 */
	public static function stringStartsWith($string, $startsWith)
	{
		if(\is_array($startsWith))
		{
			foreach($startsWith as $startsWithEntry)
			{
				if(self::stringStartsWith($string, $startsWithEntry))
				{
					return true;
				}
			}
			return false;
		}
		
		$len = \mb_strlen($startsWith);
		if($len === 0)
		{
			return true;
		}
		if($len > \mb_strlen($string))
		{
			return false;
		}
		return (\mb_substr($string, 0, $len) === $startsWith);
	}
	
	/**
	 * @param string       $string
	 * @param string|array $startsWith
	 *
	 * @return bool
	 */
	public static function stringStartsWithIgnoreCase($string, $startsWith)
	{
		if(\is_array($startsWith))
		{
			foreach($startsWith as $startsWithEntry)
			{
				if(self::stringStartsWithIgnoreCase($string, $startsWithEntry))
				{
					return true;
				}
			}
			return false;
		}
		
		$len = \mb_strlen($startsWith);
		if($len === 0)
		{
			return true;
		}
		if($len > \mb_strlen($string))
		{
			return false;
		}
		return (\mb_strtolower(\mb_substr($string, 0, $len)) === \mb_strtolower($startsWith));
	}
	
	/**
	 * @param string       $string
	 * @param string|array $endsWith
	 *
	 * @return bool
	 */
	public static function stringEndsWith($string, $endsWith)
	{
		if(\is_array($endsWith))
		{
			foreach($endsWith as $startsWithEntry)
			{
				if(self::stringEndsWith($string, $startsWithEntry))
				{
					return true;
				}
			}
			return false;
		}
		
		$len = \mb_strlen($endsWith);
		if($len === 0)
		{
			return true;
		}
		if($len > \mb_strlen($string))
		{
			return false;
		}
		return (\mb_substr($string, -$len) === $endsWith);
	}
	
	/**
	 * @param string       $string
	 * @param string|array $endsWith
	 *
	 * @return bool
	 */
	public static function stringEndsWithIgnoreCase($string, $endsWith)
	{
		if(\is_array($endsWith))
		{
			foreach($endsWith as $startsWithEntry)
			{
				if(self::stringEndsWithIgnoreCase($string, $startsWithEntry))
				{
					return true;
				}
			}
			return false;
		}
		
		$len = \mb_strlen($endsWith);
		if($len === 0)
		{
			return true;
		}
		if($len > \mb_strlen($string))
		{
			return false;
		}
		return (\mb_strtolower(\mb_substr($string, -$len)) === \mb_strtolower($endsWith));
	}
	
	/**
	 * @param string $string
	 * @param string $needle
	 *
	 * @return bool
	 */
	public static function stringContains($string, $needle)
	{
		return (\mb_strpos($string, $needle) !== false);
	}
	
	/**
	 * @param string $string
	 * @param string $needle
	 *
	 * @return bool
	 */
	public static function stringContainsIgnoreCase($string, $needle)
	{
		return (\mb_stripos($string, $needle) !== false);
	}
	
	/**
	 * @param string $path
	 *
	 * @return string
	 */
	public static function normalizePathSlashes($path)
	{
		return \str_replace('\\', '/', $path);
	}
	
	/**
	 * @param string $path
	 *
	 * @return string
	 */
	public static function resolvePath($path)
	{
		$pathArray = [];
		$path = self::normalizePathSlashes($path);
		$parts = \explode('/', $path);
		if(empty($parts))
		{
			return '';
		}
		foreach($parts as $part)
		{
			if(($part === '') || ($part === '.'))
			{
				continue;
			}
			if($part !== '..')
			{
				$pathArray[] = $part;
			}
			else
			{
				\array_pop($pathArray);
			}
		}
		$fixedPath = \implode('/', $pathArray);
		if(self::stringStartsWith($path, '/'))
		{
			$fixedPath = '/' . $fixedPath;
		}
		if(self::stringEndsWith($path, '/'))
		{
			$fixedPath .= '/';
		}
		return $fixedPath;
	}
	
	/**
	 * @param string $filePath
	 *
	 * @return string
	 */
	public static function resolveRelativeFilePath($filePath)
	{
		$pathArray = [];
		$filePath = self::normalizePathSlashes($filePath);
		$parts = \explode('/', $filePath);
		if(empty($parts))
		{
			return '';
		}
		foreach($parts as $part)
		{
			if(($part === '') || ($part === '.'))
			{
				continue;
			}
			if($part !== '..')
			{
				$pathArray[] = $part;
			}
			else
			{
				\array_pop($pathArray);
			}
		}
		return \implode('/', $pathArray);
	}
	
	/**
	 * @param string $path
	 *
	 * @return int
	 */
	public static function getRelativePathDepth($path)
	{
		$pathDepth = 0;
		$path = self::normalizePathSlashes($path);
		$parts = \explode('/', $path);
		if(empty($parts))
		{
			return 0;
		}
		foreach($parts as $part)
		{
			if(($part === '') || ($part === '.'))
			{
				continue;
			}
			if($part !== '..')
			{
				$pathDepth++;
			}
			elseif($pathDepth > 0)
			{
				$pathDepth--;
			}
		}
		return $pathDepth;
	}
	
	/**
	 * @param string $path
	 */
	public static function fixFilePermissions($path)
	{
		if(!self::isWindowsOs())
		{
			$path = self::normalizePathSlashes($path);
			try
			{
				@\chmod($path, 0777);
			}
			catch(\Throwable $e)
			{
			}
			try
			{
				@\chown($path, 'www-data');
			}
			catch(\Throwable $e)
			{
			}
		}
	}
	
	/**
	 * @param string $filePath
	 *
	 * @return string
	 */
	public static function mb_basename($filePath)
	{
		$filePath = self::normalizePathSlashes($filePath);
		$parts = \explode('/', $filePath);
		if(empty($parts))
		{
			return '';
		}
		return \end($parts);
	}
	
	/**
	 * @param string $filePath
	 *
	 * @return string
	 */
	public static function mb_dirname($filePath)
	{
		$filePath = self::normalizePathSlashes($filePath);
		$parts = \explode('/', $filePath);
		if(empty($parts))
		{
			return '';
		}
		\array_pop($parts);
		return \implode('/', $parts) . '/';
	}
	
	/**
	 * @return string
	 * @throws \RuntimeException
	 */
	public static function getPhpBinary()
	{
		if($php = \getenv('PHP_BINARY'))
		{
			if(@\is_executable($php))
			{
				if(($sapi = \getenv('PHP_SAPI')) && \is_array($sapi) && \in_array($sapi, ['cli', 'cli-server', 'phpdbg'], true))
				{
					return $php . ' -qrr';
				}
				return $php;
			}
		}
		if($php = \getenv('PHP_PATH'))
		{
			if(@\is_executable($php))
			{
				return $php;
			}
		}
		if($php = \getenv('PHP_PEAR_PHP_BIN'))
		{
			if(@\is_executable($php))
			{
				return $php;
			}
		}
		if($phpdir = \getenv('PHP_BINDIR'))
		{
			$php = $phpdir . (self::isWindowsOs() ? '/php.exe' : '/php');
			if(@\is_executable($php))
			{
				return $php;
			}
		}
		throw new \RuntimeException('can\'t find the php binary');
	}
	
	/**
	 * @param string $path
	 *
	 * @throws \Throwable
	 */
	public static function gzipCompressFile($path)
	{
		$path = self::normalizePathSlashes($path);
		$pathDest = $path . '.gz';
		$mode = 'wb9';
		self::fixFilePermissions($path);
		self::fixFilePermissions(self::mb_dirname($pathDest));
		self::fixFilePermissions($pathDest);
		if($out = \gzopen($pathDest, $mode))
		{
			try
			{
				if($in = \fopen($path, 'rb'))
				{
					try
					{
						while(!\feof($in))
						{
							$inRead = \fread($in, 1024 * 1024 * 10);
							if($inRead === false)
							{
								throw new \RuntimeException('Reading file "' . $path . '" failed');
							}
							\gzwrite($out, $inRead);
						}
						\fclose($in);
					}
					catch(\Throwable $e2)
					{
						\fclose($in);
						throw $e2;
					}
				}
				else
				{
					throw new \RuntimeException('Reading file "' . $path . '" failed');
				}
				\gzclose($out);
			}
			catch(\Throwable $e)
			{
				\gzclose($out);
				try
				{
					self::deleteFile($pathDest);
				}
				catch(\Throwable $e2)
				{
				}
				throw $e;
			}
		}
		else
		{
			throw new \RuntimeException('Creating file ' . $pathDest . '" failed');
		}
	}
	
	/**
	 * @param string $string
	 *
	 * @return string
	 */
	public static function increaseNumericStringByOne($string)
	{
		$string = \trim($string);
		if(($string === '') || !\ctype_digit($string))
		{
			return '1';
		}
		for($i = \strlen($string) - 1; $i >= 0; $i--)
		{
			if(((int) $string[$i]) < 9)
			{
				$string[$i] = (string) (((int) $string[$i]) + 1);
				break;
			}
			$string[$i] = '0';
		}
		if(($string[0] === '0') || ($string[0] === 0))
		{
			$string = '1' . $string;
		}
		return $string;
	}
	
	/**
	 * @param string $string
	 *
	 * @return string
	 */
	public static function getHash($string)
	{
		return \md5($string, false);
	}
	
	/**
	 * @param string $file
	 *
	 * @return false|string
	 */
	public static function getFileHash($file)
	{
		return \md5_file($file, false);
	}
	
	/**
	 * @param array         $filelist  (map of [$relativePath => $fullFilePath])
	 * @param array|boolean $filetypes (array of accepted extensions, or pass true to accept all file extensions)
	 *
	 * @return string
	 */
	public static function getFilesMapHash($filelist, $filetypes = true)
	{
		if(!\is_array($filelist))
		{
			throw new \RuntimeException('invalid array has been given: ' . \json_encode($filelist, \JSON_PARTIAL_OUTPUT_ON_ERROR));
		}
		$hashes = [];
		foreach($filelist as $relativePath => $fullFilePath)
		{
			$relativePath = self::normalizePathSlashes($relativePath);
			$fullFilePath = self::normalizePathSlashes($fullFilePath);
			if(\is_dir($fullFilePath))
			{
				$relativePath = \rtrim($relativePath, '/');
				$filesData = System::loopThroughDirRecursive($fullFilePath);
				foreach($filesData as $fileData)
				{
					//$relativeDirPath = $fileData[0];
					//$fileName = $fileData[1];
					//$fullFilePath = $fileData[2];
					self::getFilesMapHash_processResource($relativePath . $fileData[0], $fileData[1], $fileData[2], $hashes, $filetypes);
				}
			}
			else
			{
				$fileName = System::mb_basename($relativePath);
				$relativeDirPath = System::mb_dirname($relativePath);
				self::getFilesMapHash_processResource($relativeDirPath, $fileName, $fullFilePath, $hashes, $filetypes);
			}
		}
		return self::getHash(implode('', $hashes));
	}
	
	private static function getFilesMapHash_processResource($relativeDirPath, $fileName, $fullFilePath, &$hashes, $filetypes)
	{
		if(!\is_file($fullFilePath))
		{
			throw new \RuntimeException('could not find file: ' . $fullFilePath);
		}
		$accepttype = false;
		if(\is_array($filetypes))
		{
			$accepttype = \in_array(self::getFileExtension($fileName), $filetypes, true);
		}
		if(($accepttype === true) || ($filetypes === true))
		{
			self::fixFilePermissions($fullFilePath);
			$hashes[] = self::getHash(System::resolveRelativeFilePath($relativeDirPath . $fileName));
			$hashes[] = self::getFileHash($fullFilePath);
		}
	}
	
	/**
	 * @param array|string  $filelist  (array of $fullFilePath, if a single string has been given, it will be converted to an array automatically)
	 * @param array|boolean $filetypes (array of accepted extensions, or pass true to accept all file extensions)
	 *
	 * @return string
	 */
	public static function getFilesArrayHash($filelist, $filetypes = true)
	{
		if(!\is_array($filelist))
		{
			if(!\is_string($filelist))
			{
				throw new \RuntimeException('invalid array or string has been given: ' . \json_encode($filelist, \JSON_PARTIAL_OUTPUT_ON_ERROR));
			}
			$filelist = [$filelist];
		}
		$hashes = [];
		foreach($filelist as $fullFilePath)
		{
			$fullFilePath = self::normalizePathSlashes($fullFilePath);
			if(\is_dir($fullFilePath))
			{
				$filesData = System::loopThroughDirRecursive($fullFilePath);
				foreach($filesData as $fileData)
				{
					//$relativeDirPath = $fileData[0];
					//$fileName = $fileData[1];
					//$fullFilePath = $fileData[2];
					self::getFilesArrayHash_processResource($fileData[2], $hashes, $filetypes);
				}
			}
			else
			{
				self::getFilesArrayHash_processResource($fullFilePath, $hashes, $filetypes);
			}
		}
		return self::getHash(implode('', $hashes));
	}
	
	private static function getFilesArrayHash_processResource($fullFilePath, &$hashes, $filetypes)
	{
		if(!\is_file($fullFilePath))
		{
			throw new \RuntimeException('could not find file: ' . $fullFilePath);
		}
		$accepttype = false;
		if(\is_array($filetypes))
		{
			$accepttype = \in_array(self::getFileExtension($fullFilePath), $filetypes, true);
		}
		if(($accepttype === true) || ($filetypes === true))
		{
			self::fixFilePermissions($fullFilePath);
			$hashes[] = self::getHash($fullFilePath);
			$hashes[] = self::getFileHash($fullFilePath);
		}
	}
	
	/**
	 * @param array $array1
	 * @param array $array2
	 *
	 * @return array
	 */
	public static function array_merge_recursive_distinct(array $array1, array $array2)
	{
		$merged = $array1;
		foreach($array2 as $key => $value)
		{
			if(\is_array($value) && isset($merged[$key]) && \is_array($merged[$key]))
			{
				$merged[$key] = self::array_merge_recursive_distinct($merged[$key], $value);
			}
			else
			{
				$merged[$key] = $value;
			}
		}
		return $merged;
	}
	
	/**
	 * @param string $contentA
	 * @param string $contentB
	 *
	 * @return bool
	 */
	public static function isContentEqual($contentA, $contentB)
	{
		return (\trim(\str_replace('\\', '', '' . $contentA)) === \trim(\str_replace('\\', '', '' . $contentB)));
	}
	
	/**
	 * @param string $fileName
	 *
	 * @return string
	 */
	public static function getFileExtension($fileName)
	{
		// return \mb_strtolower(\pathinfo($fileName, \PATHINFO_EXTENSION));
		
		// to ensure UTF-8 compatibility (plus it's also slightly faster):
		$pos = \mb_strrchr($fileName, '.');
		return (($pos === false) ? '' : \mb_strtolower(\mb_substr($pos, 1)));
	}
}
